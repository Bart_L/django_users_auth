from django.shortcuts import render
from basic_app.forms import  UserForm, UserProfileInfoForm

# imports for login
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    return render(request, 'basic_app/index.html')

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):

    registered = False

    if request.method == "POST":
        # grab info from forms
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileInfoForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            # saving user from form
            user = user_form.save()
            # hashing password
            user.set_password(user.password)
            # saving udpated user with hashed password
            user.save()

            # don't save yet (see below)
            profile = profile_form.save(commit=False)
            # sets onetoone relationship
            profile.user = user

            if 'profile_pic' in request.FILES:
                profile.profile_pic = request.FILES['profile_pic']

            profile.save()

            registered = True
        else:
            print(user_form.errors, profile_form.errors)

    else:
        user_form =  UserForm()
        profile_form = UserProfileInfoForm()

    return render(request, 'basic_app/registration.html',
                            {'user_form': user_form,
                             'profile_form': profile_form,
                             'registered': registered})


def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:

            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("ACCOUNT NOT ACTIVE")

        else:
            print("SOMEONE TRIED TO LOGIN AND FAILED")
            print(f"Username: {username} and password {password}")
            return HttpResponse("Invalid login details supplied!")

    else:
        return render(request, 'basic_app/login.html', {})
